Improved Training of Wasserstein GANs
=====================================

Code for reproducing experiments in ["Improved Training of Wasserstein GANs"](https://arxiv.org/abs/1704.00028).</br>

利用WGAN进行问卷数据集的对抗生成实验


## Prerequisites

- Python, NumPy, TensorFlow, SciPy, Matplotlib
- A recent NVIDIA GPU

## Models

Configuration for all models is specified in a list of constants at the top of
the file. Two models should work "out of the box":

- `python gan_toy.py`: Toy datasets (8 Gaussians, 25 Gaussians, Swiss Roll).
- `python gan_mnist.py`: MNIST

For the other models, edit the file to specify the path to the dataset in
`DATA_DIR` before running. Each model's dataset is publicly available; the
download URL is in the file.

- `python gan_language_big5.py`: Character-level language model for big5 dataset
- `python gan_language.py`: Character-level language model

## Hyperparameter

big5心理问卷数据集设置

- `BATCH_SIZE`: 64
- `ITERS`: 10000
- `SEQ_LEN`: 49
- `DIM`: 20
- `CRITIC_ITERS`: 10
- `LAMBDA`: 10
- `MAX_N_EXAMPLES`: 7187
- `FILE_PATH`: './save/big5/'
- `filter_size`: 10 per 10 questions as a group
